console.log('Lesson 8 is here...');

const data =  fetch('https://prostoevgeniy.gitlab.io/lesson_8_fetch/js/data.json');
let newdata, bankBranch = [], officeBuilding = [], carMarket = [];

data.then((res) => res.json())
	.then(res => {
		console.log('second res ', res);
		let result = res['devices'];
		console.log('result ',result);
		for(let item of result){
			item['placeUa'] == 'Відділення банку' ? bankBranch.push(item) : 
			item['placeUa'].indexOf('фісн') >= 0 ? officeBuilding.push(item) :
			item['placeUa'].indexOf('ринок') >= 0 ? carMarket.push(item) : '';
		}

		console.log('bank branch', bankBranch);
		console.log('office building', officeBuilding);
		console.log('car market', carMarket);

		printResult([bankBranch, officeBuilding, carMarket]);

	}).catch(error => {
		console.log('error ',error);
	});
//////////////////////////////////////////////////////////
function getFullAddressUA (arr){
	let res = arr.map((el, ind) => {
		return `<<<${el['placeUa']} -- ${el['fullAddressUa']}>>>`;
	});
	return res;
}
function printResult(array){
	for(let i=0; i < array.length; i++){
		if(array[i].length == 0){
			alert('Мультибанки отсутствуют.');
		} else {
			alert(getFullAddressUA(array[i]));
		}
	}
}